FROM node:10-alpine

RUN apk update \
    && apk add --no-cache \
        ca-certificates=20190108-r0 \
        curl=7.66.0-r0
COPY pipe .

RUN npm i
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
